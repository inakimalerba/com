---
title: "talks and presentations"
layout: nodate
draft: false
---

## Advanced GitLab CI/CD for fun and profit

*@ DevConf.CZ 2022*

[Presentation](https://devconfcz2022.sched.com/event/siHv/advanced-gitlab-cicd-for-fun-and-profit),
[Video](https://www.youtube.com/watch?v=77913RmZUKk),
[Slides](https://static.sched.com/hosted_files/devconfcz2022/79/DevConf.cz%202022%20-%20Advanced%20GitLab%20CI_CD%20for%20fun%20and%20profit.pdf),
[Source](https://docs.google.com/presentation/d/1-K93FEX8tAIlUHwQo4pAyEbp1czs59JYl7ZygVUrdMI)

## From Jenkins-under-your-desk to resilient service

*@ DevConf.CZ 2021*

[Presentation](https://devconfcz2021.sched.com/event/gmIL/from-jenkins-under-your-desk-to-resilient-service),
[Video](https://www.youtube.com/watch?v=C7FLD0FUnsY),
[Slides](https://static.sched.com/hosted_files/devconfcz2021/59/DevConf.cz%202021%20-%20From%20Jenkins-under-your-desk%20to%20resilient%20service.pdf)

## Team Intro

*@ Cyborg Infrastructure Workshop 2021*

[Presentation](https://cki-project.org/news/2021-01-14-infra-workshop/),
[Video](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-cki-team-intro.odp),
[Slides](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-cki-team-intro.pdf)

## How the CKI team keeps its service running

*@ Cyborg Infrastructure Workshop 2021*

[Presentation](https://cki-project.org/news/2021-01-14-infra-workshop),
[Video](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-how-do-you-run-your-service.odp),
[Slides](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-how-do-you-run-your-service.pdf)

## How the CKI team hacks on its service

*@ Cyborg Infrastructure Workshop 2021*

[Presentation](https://cki-project.org/news/2021-01-14-infra-workshop),
[Video](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-how-do-you-hack-on-your-service.odp),
[Slides](https://cki-project.org/news/2021-01-14-infra-workshop/2021-cyborg-infra-workshop-how-do-you-hack-on-your-service.pdf)

## Where is Salsa CI right now?

*@ DebConf 2020*

[Presentation](https://debconf20.debconf.org/talks/47-where-is-salsa-ci-right-now/),
[Video](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/47-where-is-salsa-ci-right-now.webm)


## 7 Ways to Make Kernel Developers Like Cookies

*@ DevConf.CZ 2020*

[Presentation](https://devconfcz2020a.sched.com/event/YOqY/7-ways-to-make-kernel-developers-like-cookies),
[Video](https://www.youtube.com/watch?v=hNxcuWkvzpo),
[Slides](https://static.sched.com/hosted_files/devconfcz2020a/9a/3-30-pm-7-ways.pdf)

## Salsa CI - Debian Pipeline for Developers

*@ MiniDebConf Hamburg 2019*

[Presentation](https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg),
[Video](https://meetings-archive.debian.net/Public/debian-meetings/2019/miniconf-hamburg/salsaci.webm),
[Slides](https://wiki.debian.org/DebianEvents/de/2019/MiniDebConfHamburg?action=AttachFile&do=view&target=salsa-ci-team-slides.pdf)
