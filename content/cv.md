---
title: "cv"
layout: nodate
draft: false
---

## Arculus

Software Engineer - Foundations - Oct/2022 - present

## Red Hat

Sr Software Engineer - Oct/2019 - Sept/2022

Software Engineer - Jul/2019 - Oct/2020

## Debian

Maintainer - Aug/2018 - present

## Satellogic

Software Engineer - Jun/2016 - Feb/2019
